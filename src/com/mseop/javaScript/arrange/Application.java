package com.mseop.javaScript.arrange;

public class Application {
/*
	 javaScript  [ 배열 ]
	
	* 자바스키릅트에서는 자료형 지정이 없다.
	* 모든 자료형을 보관하는 변수의 모음을 배열로 처리한다.
	* java에서의 컬렉션과 비슷하다.
	* 모든 타입의 값을 배열 형태로 담을수 있다.
	ex) let arr1 = ['홍길동', 20, true, false, [1,2,3,4]]; 
	* 배열 선언시 크기를 지정하지 않고 선안할수 있다 
	ex) let arr1 = new Array();
	* 배열 선언시 크기를 정하고 선언할 수 있다.
	ex) let arr1 = new Array(3);
	ex) 하지만 배열의 크기가 0.1.2여도 3을 추가하면 자동적으로 늘어나 방이 만들어진다.
	* 생성자를 통한 생성도 가능하다.
	ex) let arr1 = new Array('홍길동', '임꺽정');
	ex) let arr1 = ['java', 'oracle'];
	
	
	
	배열을 생성하는 4가지 방법
	
	let arr1 = new Array();	// 빈 공간 배열
	let arr2 = new Array(5);	// 5공간의 배열
	let arr3 = [];		// 빈공간 배열
	let arr4 = [1, 2, 3, 4, 5]	// 5공간의 배열
	
	자료형이 무엇인지 확인
	ex) typeof(arr)
	
	prompt('입력창을 받아와주는 객체');
	
	concat : 두개혹은 그이상을 결합할때 사용한다.
	ex) let sum = arr1.concat(arr2);
	
	join : 두개이상을 결합한다. 문자열로 반환한다.
	ex) let arrJoin = arr.join();
	
	indexOf : 배열의 인덱스 몇번째있는지 찾아오기
	ex) let arr = ['사과', '딸기'] 
	ex)      arr.indexOf()
	
	reverse() : 배열의 순서를 뒤집는다.
	ex) arr.revers()
	
	soft() : 배열의 내림차순 & 오름차순으로 정렬한다.
	오름차순 : 숫자 크기대로 정렬 X , 배열에 있는 내용을 문자열로 바꿔 아스키코드 문자의 대한 순서로 정렬이됨.
	내림차순 : 두 번째 인수가 더큰 경우 양수값을 리턴하게 순서를 바꾼다.
	
	push() : 배열의 맨 뒤에 요소 추가
	ex) lett arr = ['서초동', '방배동']; 
	          arr.push('천호동')
	
	pop() : 배열의 맨 뒤에 요소를 제거후 리턴,
	ex) lett arr = ['서초동', '방배동']; 
	          arr.pop();
	
	unshift() : 배열의 맨 앞에 요소 추가 
	ex) let arr = ['야구', '축구']
	         arr.unshift('당구')
					결과값 : '당구','야구', 축구
	shift() : 배열의 맨 앞에 요소 제거
	ex) let arr = ['야구','축구']
	         arr.shift()
					결과값 : 야구', '축구'
	
	slice() : 배열의 요소를 선택해서 잘라낸다.
	ex) arr.slice(2,4) 시작인덱스/종료인덱스
	
	splice() : splice([index], 제거수, 추가값]
	ex) arr.splic(2,2,'spring')
	
	toString() : 배열을 문자열로 반환한다.
	
	
	Math.pow(3,2)	// 9,	3의 2승
	Math.round(10.6);	// 11,	10.6을 반올림
	Math.ceil(10.2);	// 11,	10.2을 올림
	Math.floor(10.6);	// 10,	10.6을 내림
	Math.sqrt(9)	// 3,	3의 제곱근
	Math.random();	//	0 부터 1.0 사이의 랜덤한 숫자
 * */
}
